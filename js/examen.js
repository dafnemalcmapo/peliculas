document.getElementById('btnBuscar').addEventListener('click', function() {
    const nombrePelicula = document.getElementById('nombre').value.toLowerCase(); 
    const apiUrl = `http://www.omdbapi.com/?t=${nombrePelicula}&plot=full&apikey=30063268`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            if (nombrePelicula === data.Title.toLowerCase()) {
                document.getElementById('infoNombre').innerText = `${data.Title}`;
                document.getElementById('infoYear').innerText = `${data.Year}`;
                document.getElementById('infoActores').innerText = `${data.Actors}`;
                document.getElementById('infoResena').value = `${data.Plot}`;
                document.getElementById('infoPoster').src = data.Poster;
            } else {
                document.getElementById('infoNombre').innerText = '';
                document.getElementById('infoYear').innerText = '';
                document.getElementById('infoActores').innerText = '';
                document.getElementById('infoResena').value = '';
                document.getElementById('infoPoster').src = '';
                alert('No se encontró ninguna película con el título ingresado. Intente de nuevo.');
            }
        })
        .catch(error => console.error('Error al obtener datos:', error));
});
